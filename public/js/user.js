function inituser(){
    
    var user_email = '';
    var check_user='';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='userface'>" + 'my Profile' + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            document.getElementById('namee').innerHTML = user.email;
            check_user=user.email;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert('Logout!');
                }).catch(e => alert(e.message));
            });


            document.getElementById('username').innerHTML = user.email;

            
            var btnUser = document.getElementById('userface');
           
            btnUser.addEventListener('click', function(){
                location.href='userpage.html';
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            
            location.href='start.html';
        }
    });


    var orangeRef = firebase.database().ref('com_list');
    

    // List for store posts html
    var orange_post = [];
    // Counter for checking history post update complete
    
    var orange1 = 0;
    // Counter for checking when to update new post
    var orange2 = 0;


    orangeRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var i=0;
            

            snapshot.forEach(function(childSnapshot){
                if(childSnapshot.val().email===check_user){
                    orange1++;
                    orange_post.push(childSnapshot.val().title+'<br>');
                    
                    
                }
                i++;
            })

            document.getElementById('postever').innerHTML = orange_post.join('');
            

            
        })
        .catch(e => console.log(e.message));

}


window.onload = function () {
    inituser();
};