window.noty=0;
function init() {
    var user_email = '';
    document.getElementById('comment_list').innerHTML='';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='userface'>" + 'my Profile' + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            document.getElementById('namee').innerHTML = user.email;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert('Logout!');
                }).catch(e => alert(e.message));
            });

            
            var btnUser = document.getElementById('userface');
            btnUser.addEventListener('click', function(){
                location.href='userpage.html';
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            location.href='start.html';
        }
    });





    document.getElementById('first-line').innerHTML = 'New Post';
    document.getElementById('notitle').innerHTML = 'Title:';
    
    
    post_title="<textarea class='col-11 form-control' id='title' style='resize:none' rows='1'></textarea>";
    document.getElementById('textfortitle').innerHTML = post_title;

    document.getElementById('button-submit').innerHTML ='';
    btn_submit="<button id='post_btn' type='button' class='btn btn-success'>Submit</button>";
    document.getElementById('button-submit').innerHTML = btn_submit;


    
    post_btn = document.getElementById('post_btn');
    post_title=document.getElementById('title');
    post_txt = document.getElementById('comment');

    
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && post_title.value!="") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            window.noty=1;
            var com_listRef = firebase.database().ref("/com_list/");
            var user = firebase.auth().currentUser;
            var d=new Date();
            var n = d.toDateString();
            com_listRef.push().set({
                email: user.email,
                title: post_title.value,
                data: post_txt.value,
                date: n
            }).then(function(){
                post_txt.value = '';
                post_title.value='';
            }).catch(function(){
                alert('post failed.');
            })
        }
        else if(post_title.value===""){
            window.noty=0;
            alert('please enter title');
        }
        else if(post_txt.value === "" ){
            window.noty=0;
            alert('please enter content');
        }
        
    });

    // The html code for post
    
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_title="<div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";    
    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('com_list');
    

    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var i=0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var i=0;
            

            snapshot.forEach(function(childSnapshot){
                first_count++;
                total_post.push( str_before_username +childSnapshot.val().title +'</h6>'+ str_title+childSnapshot.val().email + '</strong>' +childSnapshot.val().date+'<br>'+"<button id='title"+i+"'"+"data-index='"+i+"'>"+'more'+"</button>"+str_after_content);
                i++;
            })

            document.getElementById('post_list').innerHTML = total_post.join('');
            
            var topic=0;
            for(topic=0;topic<i;topic++){
                var num='title'+topic;
                document.getElementById(num).addEventListener('click', function(){
                    
                    window.ss=this.getAttribute('data-index') ;

                    post();
                    
                    
                });
                
            };

            postsRef.on('child_added', function(childSnapshot) {
                str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
                second_count++;
                if(second_count > first_count){
                    total_post.push(str_before_username + childSnapshot.val().title +'</h6>'+str_title+childSnapshot.val().email + '</strong>'+childSnapshot.val().date+'<br>'+"<button id='title"+i+"'"+"data-index='"+i+"'>"+'more'+"</button>"+str_after_content);
                    
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    var num='title'+i;
                    document.getElementById(num).addEventListener('click', function(){
                        window.ss=this.getAttribute('data-index') ;
                        post();
                    });
                    var topic=0;
                    for(topic=0;topic<i;topic++){
                        var num='title'+topic;
                        document.getElementById(num).addEventListener('click', function(){
                            
                            window.ss=this.getAttribute('data-index') ;

                            post();
                            
                            
                        });
                        
                    };
                }
            });
        

            
        })
        .catch(e => console.log(e.message));
    
    
}

window.onload = function () {
   
    init();
    
};



function post(){
    document.getElementById('post_list').innerHTML='';

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_title="<div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";    
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    

    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var temp_post=[];
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var i=0;
    
    var click_post_title='';
    postsRef.once('value')
        .then(function(snapshot){
        snapshot.forEach(function(childSnapshot){
            if(i==window.ss){
                total_post.push( str_before_username +childSnapshot.val().title +'</h6>'+ str_title+childSnapshot.val().email + '</strong>' +childSnapshot.val().date+'<br>'+childSnapshot.val().data+str_after_content);
                click_post_title=childSnapshot.val().title;
                return true;
            }
            i++;
        })


        var button="<button id='backk'>click me back to post list</button>";
        total_post.push(button);
        document.getElementById('post_list').innerHTML = total_post.join('');
        
        
        var s='backk';
        document.getElementById(s).addEventListener('click', function(){
            init();
        });


    })
    .catch(e => console.log(e.message));

    document.getElementById('first-line').innerHTML = 'Leave Comments';
    document.getElementById('notitle').innerHTML = '';
    document.getElementById('textfortitle').innerHTML = '';

    post_txt = document.getElementById('comment');


    cmt_submit="<button id='comment_btn' type='button' class='btn btn-success'>Submit</button>";
    document.getElementById('button-submit').innerHTML = '';
    document.getElementById('button-submit').innerHTML = cmt_submit;
    post_btn = document.getElementById('comment_btn');

    
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            window.noty=1;
            var orangeRef = firebase.database().ref("/orangename/");
            var user = firebase.auth().currentUser;
            orangeRef.push().set({
                email: user.email,
                title: click_post_title,
                data: post_txt.value
            }).then(function(){
                post_txt.value = '';
            }).catch(function(){
                alert('post failed.');
            })
        }
        else if(post_txt.value === "" ){
            alert('please enter content');
            window.noty=0;
        }
        
    });








    var orangeRef = firebase.database().ref('orangename');
    

    // List for store posts html
    var orange_post = [];
    // Counter for checking history post update complete
    
    var orange1 = 0;
    // Counter for checking when to update new post
    var orange2 = 0;


    orangeRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var i=0;
            

            snapshot.forEach(function(childSnapshot){
                if(childSnapshot.val().title===click_post_title){
                    orange1++;
                    orange_post.push( str_before_username +'comment' +'</h6>'+ str_title+childSnapshot.val().email + '</strong>' +childSnapshot.val().data+str_after_content);
                    i++;
                }
            })

            document.getElementById('comment_list').innerHTML = orange_post.join('');
            
            

            orangeRef.on('child_added', function(childSnapshot) {
                str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
                if(childSnapshot.val().title===click_post_title){
                    orange2++;
                    if(orange2 > orange1){
                    
                        orange_post.push(str_before_username + 'comment' +'</h6>'+str_title+childSnapshot.val().email + '</strong>'+childSnapshot.val().data+str_after_content);
                        
                        document.getElementById('comment_list').innerHTML = orange_post.join('');
                    
                    }
                }
            });
        

            
        })
        .catch(e => console.log(e.message));

}


window.addEventListener('load', function () {
    // At first, let's check if we have permission for notification
    // If not, let's ask for it
    if (Notification && Notification.permission !== "granted") {
      Notification.requestPermission(function (status) {
        if (Notification.permission !== status) {
          Notification.permission = status;
        }
      });
    }
    var button = document.getElementById('button-submit');
    button.addEventListener('click', function () {
      // If the user agreed to get notified
      if (Notification && Notification.permission === "granted") {
        if(window.noty===1){var n = new Notification("post success");}
      }
      // If the user hasn't told if he wants to be notified or not
      // Note: because of Chrome, we are not sure the permission property
      // is set, therefore it's unsafe to check for the "default" value.
      else if (Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {
          if (Notification.permission !== status) {
            Notification.permission = status;
          }
          // If the user said okay
          if (status === "granted") {
            if(window.noty===1){var n = new Notification("post success");}
          }
          // Otherwise, we can fallback to a regular modal alert
          
        });
      }
      // If the user refuses to get notified
      
    });
  });