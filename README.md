# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
* Other functions (add/delete)
    1. show post time
    2. 可以防止空白貼文
    3. 顯示留言者email

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
作品網址: https://105000013.gitlab.io/Midterm_Project

1. 首先會先進入一個start.html的介面，若已經登入則不會看到這個介面，直接跳入post list page。CSS Animation的使用是放在start.html裡，標題會從右邊滑進。
2. 點start button會進入sign in/up的介面，可以用信箱(Membership Mechanism)或是google(third-party)登入。
3. 進入post list page可以看到全部的貼文，包含貼文的title、po文者以及po文的時間。拉到最底下可以新增貼文，且規定貼文的title跟內容都不能空白。若要看post的內容，按下more button，會進入貼文裡面。
4. 進入貼文裡可以看到貼文內容，以及之前的留言紀錄及留言者email。拉到最底下可以留言。
5. 當成功新增貼文及留言時，會出現chrome notification(使用Notification API)。
6. 點擊上方account會出現my Profile及logout，點profile就會進入user page。user page裡會秀出這個使用者po過的文章。
7. 只要點擊左上方出現的使用者email，不論何時皆能返回post list page。

 
## Security Report (Optional)

在realtime database限制只有當下登入者可以改變資料的讀寫。另外登入者的帳號密碼由firebase authentication處理，開發者不會知道其使用者的帳密。